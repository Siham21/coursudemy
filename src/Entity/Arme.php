<?php

namespace App\Entity;


class Arme {

    private $nom;
    private $description;
    private $degat;

    public static $armes = [];

    public function __construct($nom, $description, $degat)
    {
        $this->nom = $nom;
        $this ->description = $description;
        $this->degat= $degat;
        self :: $armes[] = $this;
        
    }

    public static function creerArme(){

        new Arme("epee", "Une superbe épée tranchante", 10);
        new Arme("hache", "Une arme ou un outil", 15);
        new Arme("arc", "Une arme à distance ", 7);
    }

        public static function getArmeParNom($nom){

            foreach (self::$armes as $arme){
                if($arme->nom === $nom){
                    return $arme;
                }
            }
        }










    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of degat
     */ 
    public function getDegat()
    {
        return $this->degat;
    }

    /**
     * Set the value of degat
     *
     * @return  self
     */ 
    public function setDegat($degat)
    {
        $this->degat = $degat;

        return $this;
    }
}





